﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RSExt
{
    public class Extension
    {
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int MessageBox(int hWnd, String text, String caption, uint type);
        static string prefix = "test01";
        public string CommandPrefix()
        {
            return prefix;
        }

        public int Command(string cmd)
        {
            int rcode = 0;
            System.IO.File.WriteAllText("__t", $"Ok {prefix}:{cmd}\n");
            rcode = MessageBox(0, $"{prefix}:{cmd}", "Ext Infp", 0);
            //System.Windows.Forms.MessageBox.Show($"{prefix}:{cmd}", "Ext Infp");
            if (cmd == "exc")
            {
                throw new Exception("exc"); 
            }
            return rcode;
        }
    }
}
