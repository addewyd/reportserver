﻿using System;
using WebSocketSharp;
using WebSocketSharp.Server;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportServer
{
    // .......................................................................................

    public class Reporter : WebSocketBehavior
    {
        public Reporter()
        {
        }

        public void Resend(string m)
        {
            if (ConnectionState == WebSocketState.Open)
            {
                Send(m);
                window.AddLQ(m);
            }
        }

        public HttpServer httpsv { get; set; }
        public MainWindow window { get; set; }

        // .......................................................................................

        protected override void OnMessage(MessageEventArgs e)
        {
            var qs = Context.QueryString;
            if (e.Data == "message")
            {
                // reserved. Do nothing
            }
            else if (e.Data == "open")
            {
                Send($@"{{""ID"": ""{ID}""}}");
                Connections.log.Info($"Open {ID}");
            }
            else
            {
                string[] acmd = System.Text.RegularExpressions.Regex.Split(e.Data, @"\:");

                if (!(acmd.Length > 1 && acmd[1].Length > 0))
                {
                    // it is not command
                    return;
                }
                string command = "";

                if (acmd[0] == "std")
                {
                    // standsrd
                    command = acmd[1];
                    lock (Connections._lock)
                    {
                        // Save last command
                        Connections.command = command;
                    }
                    Connections.log.Info($"std command {command} saved");
                    window.AddLQ(command);

                    return;
                }

                // here extensions
                string prefix = acmd[0];
                string extcommand = acmd[1];
                if (Connections.Exts.ContainsKey(prefix))
                {
                    try
                    {
                        var mi = Connections.Exts[prefix];
                        var opCmd = Activator.CreateInstance(mi.ReflectedType);
                        object[] parameters = new object[1];
                        parameters[0] = extcommand;

                        // return code
                        object extrc = mi.Invoke(opCmd, parameters);

                        Connections.log.Info($"Invoke ext {prefix}:{extcommand} rc: {Convert.ToInt32(extrc)}");
                        window.AddLQ($"Invoke ext {prefix}:{extcommand} rc: {Convert.ToInt32(extrc)}");
                    }
                    catch (Exception ex)
                    {
                        //MsgWin.Msg(ex.Message);
                        // log better
                        Connections.log.Error($"In ext: {ex.Message}\n{ex.StackTrace}");
                    }
                }
                else
                {
                    Connections.log.Info($"Unknown command prefix {prefix} command {extcommand}");
                    window.AddLQ($"Unknown command {prefix}:{command}");
                }
            }
        }

        // ....................................................................

        protected override void OnOpen()
        {
            Connections.conns.Add(ID, this);
            httpsv.Log.Info($"added conn with id {ID}");
            window.AddLQ($"Connection {ID} added");
        }

        // ....................................................................

        protected override void OnClose(CloseEventArgs e)
        {
            Connections.conns.Remove(ID);
            lock (Connections._lockQL)
            {
                window.AddLQ($"Connection {ID} closing");
            }
        }

        // ....................................................................

        protected override void OnError(ErrorEventArgs e)
        {
            window.AddLQ($"Error {e.Message}");
            Console.WriteLine($"Error {e.Message}");
        }
    }
}
