﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ReportServer
{
    public class MainWindow : Form
    {
        private const int WM_CLOSE = 0x10;
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        private Queue<string> LogQ = new Queue<string>(8);

        public MainWindow()
        {
        }

        public void AddLQ(string s)
        {
            ListBox c = Controls["logview"] as ListBox;
            if (c == null) return;
            lock (Connections._lockQL)
            {
                LogQ.Enqueue(s);
                if (LogQ.Count > 8)
                {
                    LogQ.Dequeue();
                }
                int cnt = 0;

                foreach (string i in LogQ)
                {
                    if (c.Items.Count < cnt + 1)
                    {
                        c.Items.Add(i);
                    }
                    else
                    {
                        c.Items[cnt] = i;
                    }
                    cnt++;
                }
            }
        }

        // .....................................................................................

        protected override void WndProc(ref Message m)
        {
            try
            {
                if (m.Msg == WM_CLOSE)
                {
                    //MsgWin.Msg("Close MSG");
                    Connections.log.Info("Shut via Close");
                    Application.ExitThread();
                    return;
                }
                using (var brw = new SharedMemory.BufferReadWrite(name: Connections.SBName))
                {
                    brw.Read<int>(out int readData);
                    if (readData == 900)
                    {
                        Connections.log.Info("Shut via brw");
                        //brw.Dispose();
                        readData = 0;
                        brw.Write<int>(ref readData);
                        //Application.ExitThread();
                        SendMessage(this.Handle, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
                    }
                }
                base.WndProc(ref m);
            }
            catch (Exception ex)
            {
                MsgWin.Msg(ex.Message);
            }
        }
    }
}
