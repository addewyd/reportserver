﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WebSocketSharp;
using WebSocketSharp.Net;
using WebSocketSharp.Server;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ReportServer
{ 
    public static class Connections {
        public static string SBName = "99277304-47F0-4400-A522-5AA19E0B251C";
        public static string command = "";
        public static readonly object _lock = new object();
        public static readonly object _lockL = new object();
        public static readonly object _lockQL = new object();

        // active connections
        public static Dictionary<string, Reporter> conns = new Dictionary<string, Reporter>();
        // loaded extensions
        public static Dictionary<string, System.Reflection.MethodInfo> Exts = new Dictionary<string, System.Reflection.MethodInfo>();
        public static Logger log = null;
    }

    // ..........................................................................................

    public static class MsgWin
    {
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int MessageBox(int hWnd, String text, String caption, uint type);
        public static void Msg(string m)
        {
            MessageBox(0, m, "Info", 0);
        }
    }

    // .......................................................................................

    static class Program
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AllocConsole();

        [DllImport("kernel32", SetLastError = true)]
        static extern bool AttachConsole(int dwProcessId);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int MessageBox(int hWnd, String text, String caption, uint type);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        private const int WM_CLOSE = 0x10;

        static HttpServer httpsv = null;

        static NotifyIcon _ni = null;
        static MainWindow window = null;

        // .......................................................................

        static void Resend(string msg)
        {
            foreach(var kv in Connections.conns)
            {
                kv.Value.Resend(msg);
            }
        }

        // .......................................................................................

        static void Run()
        {
            try
            {
                if (httpsv == null)
                {
                    httpsv = new HttpServer(Int32.Parse(ConfigurationManager.AppSettings["Port"]));
                }
                httpsv.Log.File = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + ConfigurationManager.AppSettings["ProgPath"] + @"\Log.txt";
                httpsv.Log.Level = LogLevel.Trace;
                httpsv.DocumentRootPath = ConfigurationManager.AppSettings["DocumentRootPath"];
                string index = ConfigurationManager.AppSettings["index"];

                string HttpPath = ConfigurationManager.AppSettings["HttpPath"];

                // .......................................................................................

                httpsv.OnGet += (sender, e) =>
                {
                    var req = e.Request;
                    var res = e.Response;
                    var qs = req.QueryString;
                    var path = req.RawUrl;

                    if (path == "/")
                    {
                        path += index;
                    }

                    byte[] contents;

                    if (!e.TryReadFile(path, out contents))
                    {
                        res.StatusCode = (int)HttpStatusCode.NotFound;
                        return;
                    }

                    if (path.EndsWith(".html"))
                    {
                        res.ContentType = "text/html";
                        res.ContentEncoding = Encoding.UTF8;
                    }
                    else if (path.EndsWith(".js"))
                    {
                        res.ContentType = "application/javascript";
                        res.ContentEncoding = Encoding.UTF8;
                    }

                    res.ContentLength64 = contents.LongLength;
                    res.Close(contents, true);
                };

                // ................................................................................

                // post request from pix reporter client
                httpsv.OnPost += (sender, e) =>
                {
                    var req = e.Request;
                    var res = e.Response;                    
                    var path = req.RawUrl;

                    if (path == HttpPath)
                    {
                        byte[] contents = new byte[req.ContentLength64];

                        var inps = req.InputStream;
                        inps.Read(contents, 0, Convert.ToInt32(req.ContentLength64));
                        string msg = System.Text.Encoding.UTF8.GetString(contents);

                        // send info to ws client (browser)
                        Resend(msg);

                        // Repost last command back
                        byte[] b = { };
                        int len;
                        lock (Connections._lock)
                        {
                            len = Connections.command.Length;
                            if (len > 0)
                            {
                                Connections.log.Info($"Response command: {Connections.command}");
                                b = Encoding.UTF8.GetBytes(Connections.command);
                            }
                        }
                        if (len > 0)
                        {
                            res.ContentLength64 = len;
                            res.ContentType = "text/plain";
                            res.OutputStream.Write(b, 0, len);
                            res.OutputStream.Flush();
                            //   res.Close();
                            lock (Connections._lock)
                            {
                                // clear last command
                                Connections.command = "";
                            }
                        }
                        res.Close();  // ?
                    }
                    else
                    {
                    }
                    // res.Close()
                };

                // ...........................................................................

                httpsv.AddWebSocketService<Reporter>("/Reporter", (r) =>
                {
                    // calls new Reporter 
                    r.httpsv = httpsv;
                    r.window = window;
                });

                // ........................................................................

                httpsv.Start();
                if (window == null) {
                    window = new MainWindow();
                    SetupWindow();
                    window.Show();
                    window.AddLQ("Start");
                    Application.Run();
                    Connections.log.Info("Exit from Run");
                }
                httpsv.Stop();
            } catch(Exception ex)
            {
                MsgWin.Msg(ex.Message);
            }
        }

        // .................................................................................

        private static void SetupWindow()
        {
            window.Resize += _window_Resize;
            window.Icon = RSResource.NIcon;
            _ni = new NotifyIcon
            {
                Icon = RSResource.NIcon
            };
            _ni.MouseDoubleClick += _ni_MouseDoubleClick;

            var Button01 = new Button
            {
                Text = "Close"
            };
            Button01.Click += Button01_Click;
            ListBox lw = new System.Windows.Forms.ListBox
            {
                BorderStyle = BorderStyle.FixedSingle,
                Height = 140,
                Width = 240,
                Top = 40,
                Name = "logview"
            };

            lw.Items.Add("listview");

            lw.Dock = DockStyle.Top;
            Button01.Dock = DockStyle.Bottom;

            window.Controls.Add(lw);
            window.Controls.Add(Button01);
            Button01.Focus();
        }

        // ..........................................................

        private static void _ni_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            if (window != null)
            {
                window.WindowState = FormWindowState.Normal;
                window.ShowInTaskbar = true;
                _ni.Visible = false;
            }
        }

        // ..........................................................

        private static void _window_Resize(object sender, EventArgs e)
        {
            var f = sender as Form;
            if (f.WindowState == FormWindowState.Minimized)
            {
                _ni.Visible = true;
                _ni.ShowBalloonTip(3000, "tip", "Rerport Server", ToolTipIcon.Info);
               f.ShowInTaskbar = false;
            }
        }   

        // ..........................................................

        private static void Button01_Click(object sender, EventArgs e)
        {
            var b = (sender as Button);
            var p = b.Parent;
            
            PostMessage(p.Handle, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
        }

        // ..........................................................

        // on -kf command line option
        static void ForceKill(Process proc, SharedMemory.BufferReadWrite brw)
        {
            foreach(var p in Process.GetProcesses())
            {
                if(p.ProcessName == proc.ProcessName && p.Id != proc.Id)
                {
                    p.Kill();
                }
            }
            brw.Dispose();
        }

        // ..........................................................

        // on -k command line option
        static void SendTerm(SharedMemory.BufferReadWrite brw, string msg)
        {
            int data = 900;
            brw.Write<int>(ref data);
            //MsgWin.Msg(msg);
        }

        // ............................................................................................

        static void LoadExt(string path)
        {
            var ass = System.Reflection.Assembly.LoadFrom(path);
            Type t = ass.GetType("RSExt.Extension");
            var methodInfo = t.GetMethod("CommandPrefix", new Type[] {});
            if (methodInfo == null)
            {
                throw new Exception("No such method exists.");
            }
            var op = Activator.CreateInstance(t);
            string prefix = methodInfo.Invoke(op, null).ToString();

            var methodInfoCmd = t.GetMethod("Command", System.Reflection.BindingFlags.Public
                    | System.Reflection.BindingFlags.Instance,
	                null, System.Reflection.CallingConventions.Any,
	            new Type[] { typeof(string) },
                    null);

            if (methodInfoCmd == null)
            {
                throw new Exception("No such method exists.");
            }
            Connections.Exts.Add(prefix, methodInfoCmd);
            Connections.log.Info($"added ext {prefix}");
        }

        // ............................................................................................

        static void GetExts()
        {            
            string d = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

            var files =  System.IO.Directory.GetFiles(d, "Extension.*.dll");
            Connections.log.Info(String.Join("; ", files));
            foreach (var f in files)
            {
                LoadExt(f);
            }
        }

        // ............................................................................................
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Connections.log = new Logger(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + 
                    @"\" + ConfigurationManager.AppSettings["ProgPath"] + @"\rs.log");
                Connections.log.Info("Start");
                GetExts();

                SharedMemory.BufferReadWrite producer;
                try
                {   // new if not exists,
                    producer = new SharedMemory.BufferReadWrite(name: Connections.SBName, bufferSize: 1024);
                    Connections.log.Info("New shared buffer");
                } catch(Exception ex)
                {
                    // open if exists,
                    producer = new SharedMemory.BufferReadWrite(name: Connections.SBName);
                    Connections.log.Info("Open shared buffer");
                }

                string localappath =
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + ConfigurationManager.AppSettings["ProgPath"];
                if (!System.IO.Directory.Exists(localappath))
                {
                    System.IO.Directory.CreateDirectory(localappath);
                }
                var proc = Process.GetCurrentProcess();

                int id = proc.Id;
                string name = proc.ProcessName;
                var procs = Process.GetProcesses();

                if(args.Length > 0 && args[0] == "-kf")
                {
                    ForceKill(proc, producer);
                    MsgWin.Msg("Operation completed");
                    return;
                }

                if (args.Length > 0 && args[0] == "-k")
                {
                    SendTerm(producer, "Try to send exit msg. Exit");
                    return;
                }
                foreach (var p in procs)
                {
                    if (p.ProcessName == name && p.Id != id)
                    {
                        MsgWin.Msg($"{name} {id} already running");
                        return;
                    }
                }

                // Run All
                Thread t = new Thread((ThreadStart)delegate
                {
                    Run();
                })
                {
                    Name = "MainWindow message loop",
                    IsBackground = true
                };
                t.Start();
                t.Join();
                // normal  shutdown
                producer.Dispose();
                Connections.log.Info("Shutdown");
                // MsgWin.Msg($"Norm");
            }
            catch (Exception ex)
            {
                MsgWin.Msg(ex.Message);
            }
        }
    }
}
